let motifsCartes = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8]; // creation tableau cartes avec motifs//
let etatCarte = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]; // cartes non retournées
let cardReturn = []; //cartes retournées
let pairesTrouvees = 0; // nbr de paires trouvées
let nbrErreurs = 0; // nbr erreurs
let imgCarte = document.getElementById('ensemblecartes').getElementsByTagName('img'); //on récupère la variable en ciblant le html
let scoring =document.querySelector(".scoring");
let error = document.querySelector(".error");

// console.log(imgCarte)

/* --> 1. on fait boucle dans tableau <-- */
for (let i = 0; i < imgCarte.length; i++) { //on fait une boucle pour parcourir tableau qui démarre à index=0 et finit à i<imgCarte.length

    imgCarte[i].numCarte = i; // on prend index (en cours) de l'objet imgCarte, auquel on attribue un numero (propriété numCarte) qui correspond au même index
    imgCarte[i].addEventListener("click", function () {
        controlejeu(this.numCarte);
        console.log(this.numCarte)
    }
    )
};

// console.log(imgCarte)

function controlejeu(numCarte) { //fonction appelée à chaque clic sur une carte en passant en paramètre le numéro de la carte cliquée
    if (cardReturn.length < 2) { //pour rendre impossible le fait de pouvoir retourner + de deux cartes en même temps
        if (etatCarte[numCarte] == 0) { //si carte cliquée est de dos (état 0)
            etatCarte[numCarte] = 1; //on fait passer son état à 1
            cardReturn.push(numCarte); //on ajoute son numéro au tableau des cartes retournées
            majAffichage(numCarte); //mise à jour de son affichage
        };


        if (cardReturn.length == 2) { // si deux cartes sont retournées (on doit déterminer si meme motif)
            let nouvelEtat = 0;
            if (motifsCartes[cardReturn[0]] == motifsCartes[cardReturn[1]]) { //si carte index 0 correspond à carte index 1
                nouvelEtat = -1 // alors nouvel état = -1, cad carte reste visible car similitude avec une autre carte
                pairesTrouvees++; // on incrémente la variable qui compte le nombre de paires trouvées
                scoring.innerHTML=`Nombre de paires trouvées : ${pairesTrouvees}`; //en cas de paire on affiche le texte
                console.log(pairesTrouvees)            
            } else{
                nbrErreurs++; // on incrémente nbr erreurs
                error.innerHTML=`Nombre d'erreurs : ${nbrErreurs}`; // on affiche texte avec nbr erreurs
            
            }


            etatCarte[cardReturn[0]] = nouvelEtat;// sinon carte qui reprend nouvel état 0 (reste de dos)
            etatCarte[cardReturn[1]] = nouvelEtat;


            setTimeout(function () { // méthode qui appelle fonction après un nombre spécifié de millisecondes
                majAffichage(cardReturn[0]); //après temps défini, carte index 0 et carte index 1 retourne à leur état initial []
                majAffichage(cardReturn[1]);
                cardReturn = [];
                if (pairesTrouvees == 8) { //si toutes les paires trouvées, on joue fonction rejouer (voir + haut)
                    document.querySelector(".finish").style.display = "block";
                }
            }, 1000);
        }
    };
};


/* --> pour que le bouton (lors du message de victoire) réinitialise le jeu <-- */
let btn = document.querySelector(".rejouer");
console.log(btn)
btn.addEventListener("click", function () {
    location.reload();
});


function majAffichage(numCarte) { //fonction qui permet de mettre à jour affichage de la carte dont on passe le numéro en paramètre
    switch (etatCarte[numCarte]) { // si le numéro de la carte dans mon tableau etatCarte
        case 0: // carte face cachée, le code contenu dans case 0 va être exécuté si la valeur contenue dans notre variable est 0
            imgCarte[numCarte].src = "images/recto.jpg"; //laisse afficher le recto
            break; // pour sortir du switch
        case 1: //carte retournée
            imgCarte[numCarte].src = "images/carte" + motifsCartes[numCarte] + ".png"; //affiche l'image du motif correspondant
            break;
        case -1: //carte enlevée
            // imgCarte[numCarte].style.visibility="visible"; //carte reste visible
            break;
    }

};


/* --> 2. on lance notre fonction pour mélanger cartes<-- */
initialiseJeu(); //après avoir fait boucle dans tableau on fait appel à la fonction initialiseJeu (voir + bas)


function initialiseJeu() { // fonction qui mélange les numéros de motif des cartes
    for (var position = motifsCartes.length - 1; position >= 1; position--) {
        var hasard = Math.floor(Math.random() * (position + 1));
        var sauve = motifsCartes[position];
        motifsCartes[position] = motifsCartes[hasard];
        motifsCartes[hasard] = sauve;
    }
};